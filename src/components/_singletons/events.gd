extends Node2D


var debugSignals = false


# Signals:
signal power_up_touched(body, power_up)
signal power_up_used(id, power_up)
signal power_up_effect_ended(id, power_up)
signal send_round_timer(round_timer_obj)
signal new_order_appeared(order_id)
signal order_finished(order_id)
signal order_removed(order_screen_index, order_id)
signal points_changed(new_points)

signal tutorial_picked_item_from_shelf()
signal tutorial_dropped_item_on_station()
signal tutorial_delivered_normal()
signal tutorial_delivered_fragile()
signal tutorial_delivered_priority()


func _ready():
	if debugSignals:
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			elif s.args.size() == 1:
				connect(s.name, self, "__on_signal_one_args", [s.name])
			elif s.args.size() == 2:
				connect(s.name, self, "__on_signal_two_args", [s.name])
			else:
				push_error('[EventManager]! %s name has more than 2 args, debug not set up!' % s.name)


func __on_signal_no_args(name):
	print('[EventManager]: got signal %s' % name)


func __on_signal_one_args(arg, name):
	print('[EventManager]: got signal %s with arg %s' % [name, arg])


func __on_signal_two_args(arg1, arg2, name):
	print('[EventManager]: got signal %s with args %s , %s' % [name, arg1, arg2])
