extends Node


func _enter_tree():
	self.event('started')


func event(name: String, props = null):
	var props_str = JSON.print(props) if props != null else null
	print(
		'[Analytics] %s (%s)'%[
			name,
			props_str  if props != null else 'no props'
		]
	)

	if OS.has_feature('JavaScript'):
		if props != null:
			JavaScript.eval("plausible('%s', { props: %s });"%[name, props_str])
		else:
			JavaScript.eval("plausible('%s');"%name)
