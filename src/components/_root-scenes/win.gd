extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Center/Panel/VBox/BackButton.connect('pressed', self, '_on_back_pressed')
	
	$Center/Panel/VBox/Label.set_text("You've done crate!\nYou won %s€" % [STATE.get_points()])

	ANALYTICS.event('win', {points=STATE.get_points()})


func _on_back_pressed():
	STATE.reset_level_data()
	SCENE.goto_scene('menu')
