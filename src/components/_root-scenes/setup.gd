extends Control


onready var start_button = $'%StartButton'


func _ready():
	$'%StartButton'.connect('pressed', self, '_on_start_pressed')

	MULTI_INPUT.connect('players_changed', self, '_on_players_changed')

	MULTI_INPUT.reset()

func _input(event):
	if event.is_action_pressed('ui_start'):
		_on_start_pressed()


func _on_start_pressed():
	var players_count = MULTI_INPUT.get_current_players_count()
	if players_count > 0:
		var analytics_data = {
			players_count=players_count
		}
		for id in players_count:
			analytics_data[MULTI_INPUT.get_device_name_for_player(id)] = true
		ANALYTICS.event('setup', analytics_data)

		MULTI_INPUT.disable_join()

		SCENE.goto_scene('world')

func _on_players_changed(players):
	start_button.disabled = players.size() == 0
