extends Control


func _ready():
	get_node('%StartButton').connect(
		'pressed', self, '__on_start'
	)
	get_node('%TutorialButton').connect(
		'pressed', self, '__on_tutorial'
	)

func _input(event):
	if UTIL.is_debug:
		if event.is_action_pressed('debug_f1'):
			MULTI_INPUT.reset()
			MULTI_INPUT.add_player({
				device = MULTI_INPUT.WASD_DEVICE_ID,
				color = Color.white
			})
			MULTI_INPUT.add_player({
				device = 0,
				color = Color.black
			})
			STATE.set_level(ID.LEVEL_TUTORIAL)
			STATE.set_as_tutorial()
			SCENE.goto_scene('world')


func __on_start():
	STATE.set_level(ID.LEVEL_WAREHOUSE)
	SCENE.goto_scene('setup')

func __on_tutorial():
	STATE.set_level(ID.LEVEL_TUTORIAL)
	STATE.set_as_tutorial()
	SCENE.goto_scene('setup')
