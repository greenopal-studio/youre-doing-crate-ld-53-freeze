extends StaticBody


onready var type = ID.COMPONENT_TYPE_SHELF

onready var marker1 = $Marker1
onready var marker2 = $Marker2
onready var marker3 = $Marker3
onready var pos1b = $Position3D1Bottom
onready var pos1t = $Position3D1Top
onready var pos2b = $Position3D2Bottom
onready var pos2t = $Position3D2Top
onready var pos3b = $Position3D3Bottom
onready var pos3t = $Position3D3Top


var looking_at_pos = [0, 0, 0]
var items = []


func set_items(new_items: Array):
	items = new_items


func add_item(new_item: String):
	items.append(new_item)
	if new_item == ID.ITEM_EMPTY:
		return
	
	match items.size():
		1:
			add_item_obj(new_item, pos1b.get_translation())
			add_item_obj(new_item, pos1t.get_translation())
		2:
			add_item_obj(new_item, pos2b.get_translation())
			add_item_obj(new_item, pos2t.get_translation())
		3:
			add_item_obj(new_item, pos3b.get_translation())
			add_item_obj(new_item, pos3t.get_translation())


func add_item_obj(item_key: String, pos: Vector3):
	var new_item_obj = DATA.ITEMS[item_key].obj.instance()
	new_item_obj.set_rotation(get_rotation())
	new_item_obj.set_translation(pos)
	new_item_obj.set_scale(Vector3(0.25, 0.25, 0.25))
	$Items.add_child(new_item_obj)


func get_item(collision_shape_index: int) -> String:
	if collision_shape_index >= 3:
		return items[collision_shape_index - 3]
	return ID.ITEM_EMPTY


func add_selection(collision_shape_index: int, player_id: int):
	if collision_shape_index == 3:
		looking_at_pos[0] += 1
		if looking_at_pos[0] == 1 and items[0] != ID.ITEM_EMPTY:
			marker1.show()
			marker1.set_color_for_player(player_id)
	elif collision_shape_index == 4:
		looking_at_pos[1] += 1
		if looking_at_pos[1] == 1 and items[1] != ID.ITEM_EMPTY:
			marker2.show()
			marker2.set_color_for_player(player_id)
	elif collision_shape_index == 5:
		looking_at_pos[2] += 1
		if looking_at_pos[2] == 1 and items[2] != ID.ITEM_EMPTY:
			marker3.show()
			marker3.set_color_for_player(player_id)

func remove_selection(collision_shape_index: int):
	if collision_shape_index == 3:
		looking_at_pos[0] -= 1
		if looking_at_pos[0] == 0:
			marker1.hide()
	elif collision_shape_index == 4:
		looking_at_pos[1] -= 1
		if looking_at_pos[1] == 0:
			marker2.hide()
	elif collision_shape_index == 5:
		looking_at_pos[2] -= 1
		if looking_at_pos[2] == 0:
			marker3.hide()
