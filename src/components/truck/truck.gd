extends StaticBody


const MAX_ORDERS = 5
const MAX_SPEED = 5

onready var marker1 = $Marker
onready var away_timer = $AwayTimer
onready var wait_timer = $WaitTimer
onready var action_label = $'%Action'
onready var time_label = $'%Time'

var type = ID.COMPONENT_TYPE_TRUCK

var action_text = 'Ready'
var looking_at_pos = 0
var orders_stored = []
var depart = false
var arrive = false
var closed = false
var speed = 0



func _ready():
	away_timer.connect('timeout', self, '_on_timeout')
	wait_timer.connect('timeout', self, '_on_waiting_end')
	$AnimationPlayer.connect('animation_finished', self, '_on_animation_finished')
	$Error.set_error_message('Only accepts priority orders')
	
	get_tree().connect('screen_resized', self, '_on_screen_resized')


func add_selection(collision_shape_index: int, player_id: int):
	if collision_shape_index == 0 and not closed:
		looking_at_pos += 1
		if looking_at_pos == 1:
			marker1.show()
			marker1.set_color_for_player(player_id)


func remove_selection(collision_shape_index: int):
	if collision_shape_index == 0:
		looking_at_pos -= 1
		if looking_at_pos == 0:
			marker1.hide()


func add_stored_order(order_index: int) -> bool:
	if closed:
		return false

	if !STATE.get_order(order_index).is_priority:
		$Error.show()
		return false # only priority orders are allowed in the truck
	
	orders_stored.append(order_index)
	STATE.set_order_as_delivered(order_index)
	var order_data = STATE.get_order(order_index)
	match order_data.package_size:
		ID.PACKAGE_SIZE_PAPERBAG:
			STATE.add_points(10)
		ID.PACKAGE_SIZE_PACKAGE:
			STATE.add_points(25)
		ID.PACKAGE_SIZE_CRATE:
			STATE.add_points(42)
	if orders_stored.size() == 1:
		wait_timer.start()
		action_text = 'Waiting'
		action_label.set_text(action_text)
	if orders_stored.size() == MAX_ORDERS:
		closed = true
		depart = true
		marker1.hide()
		looking_at_pos = 0
		if not wait_timer.is_stopped():
			wait_timer.stop()
		$AnimationPlayer.play('Depart')
		action_text = 'Departing'
		action_label.set_text(action_text)
		time_label.set_text('--:--')
	
	return true


func _on_waiting_end():
	closed = true
	depart = true
	marker1.hide()
	looking_at_pos = 0
	$AnimationPlayer.play('Depart')
	action_text = 'Departing'
	action_label.set_text(action_text)
	time_label.set_text('--:--')


func _on_timeout():
	arrive = true
	$AnimationPlayer.play_backwards('Depart')
	action_text = 'Arriving'
	action_label.set_text(action_text)
	time_label.set_text('--:--')


func _on_animation_finished(_anim_name: String):
	if depart:
		depart = false
		orders_stored = []
		away_timer.start()
		action_text = 'Away'
		action_label.set_text(action_text)
	if arrive:
		arrive = false
		closed = false
		action_text = 'Ready'
		action_label.set_text(action_text)
		time_label.set_text('--:--')


func _on_screen_resized():
	action_label.set_text('')
	action_label.set_text(action_text)


func _input(event):
	if UTIL.is_debug and event is InputEventKey and event.is_action('debug_f3') and event.is_pressed():
		for i in MAX_ORDERS:
			add_stored_order(i)


func _physics_process(delta):
	if not away_timer.is_stopped():
		time_label.set_text(UTIL.format_time_to_minutes(away_timer.get_time_left()))
	if not wait_timer.is_stopped():
		time_label.set_text(UTIL.format_time_to_minutes(wait_timer.get_time_left()))
