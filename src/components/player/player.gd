extends KinematicBody


const DROP_OFFSET = 1.1


var drop_scene = preload('res://components/drop/drop.tscn')
var path_follow_script = preload('res://components/player/moving_item.gd')

var type = ID.COMPONENT_TYPE_PLAYER

onready var drops_anchor = $'/root/World/DropsAnchor'
onready var anim_player =  $AnimationPlayer
onready var walk_animation_player = $WalkAnimationPlayer
onready var small_item_anchor = $'%SmallItemAnchor'
onready var medium_item_anchor = $'%MediumItemAnchor'
onready var large_item_anchor = $'%LargeItemAnchor'


var id: int
var circle_material
var current_collider = null
var current_collider_shape = null
var current_held_item_id = []
var current_held_order_id = []
var current_held_node = []
var current_power_up = null
var in_progress_working_connect = null

var power_up_active = false
var dash_active = false
var juggling_active = false


func _ready():
	# prepare circle to be re-color-able
	var circle_model = $Circle.get_child(0)
	circle_material = circle_model.get_active_material(0).duplicate()
	circle_model.set_surface_material(0, circle_material)
	
	EVENTS.connect('power_up_touched', self, '_on_power_up_touched')
	EVENTS.connect('power_up_effect_ended', self, '_on_power_up_effect_ended')


func set_data(_id):
	id = _id

	var color = MULTI_INPUT.get_player_by_id(id).color
	circle_material.albedo_color = color
	circle_material.emission = color


func _on_power_up_touched(body: Node, power_up: Node):
	if body == self and current_power_up == null and not power_up_active:
		current_power_up = power_up.get_type()
		power_up.queue_free()


func _on_power_up_effect_ended(player_id: int, power_up_id: String):
	if player_id != id:
		return
	
	power_up_active = false
	match power_up_id:
		ID.POWER_UP_GHOST:
			set_collision_layer(0b00000000000000000001)
			set_collision_mask(0b00000000000000000001)
		ID.POWER_UP_DASH:
			dash_active = false
		ID.POWER_UP_JUGGLING:
			juggling_active = false
			var last_held_item_id = null
			if current_held_order_id.size() > 0:
				last_held_item_id = current_held_order_id[current_held_order_id.size() - 1]
				current_held_order_id = []
				current_held_item_id = []
			elif current_held_item_id.size() > 0:
				last_held_item_id = current_held_item_id[current_held_item_id.size() - 1]
				current_held_item_id = []
			for i in current_held_node.size():
				current_held_node[i].queue_free()
			
			current_held_node = []
			$JugglingItems.hide()
			$BaseItemAnchor.show()
			__end_juggle()
			if last_held_item_id != null:
				if last_held_item_id is int:
					start_hold_order(last_held_item_id)
				else:
					start_hold_item(last_held_item_id)


func _input(event):
	if MULTI_INPUT.get_hold_pressed_for_player(event, id):
		if current_held_item_id.size() > 0:
			if current_collider and 'type' in current_collider:
				match current_collider.type:
					ID.COMPONENT_TYPE_PACKAGING_STATION:
						var is_success = current_collider.add_stored_item(current_held_item_id[current_held_item_id.size() - 1], current_collider_shape)
						if is_success:
							if STATE.get_is_tutorial():
								EVENTS.emit_signal('tutorial_dropped_item_on_station')
							__end_hold()
					ID.COMPONENT_TYPE_FRAGILE_STATION:
						var is_success = current_collider.add_stored_item(current_held_item_id[current_held_item_id.size() - 1], current_collider_shape)
						if is_success:
							__end_hold()
					ID.COMPONENT_TYPE_SHELF:
						if juggling_active and current_held_node.size() < 5:
							var item_id = current_collider.get_item(current_collider_shape)
							if item_id != ID.ITEM_EMPTY:
								add_juggling_item(item_id)
					ID.COMPONENT_TYPE_DROP:
						if juggling_active and current_held_node.size() < 5:
							add_juggling_item(current_collider.item_id)
							current_collider.get_parent().remove_child(current_collider)
							current_collider.queue_free()
					ID.COMPONENT_TYPE_PACKAGE_STACK:
						if juggling_active and current_held_node.size() < 5:
							add_juggling_item(current_collider.current_variant)
			else:
				drop_held_item(false)
		elif current_held_order_id.size() > 0:
			if current_collider and 'type' in current_collider:
				match current_collider.type:
					ID.COMPONENT_TYPE_TRUCK:
						var is_success = current_collider.add_stored_order(current_held_order_id[current_held_order_id.size() - 1])
						if is_success:
							if STATE.get_is_tutorial():
								EVENTS.emit_signal('tutorial_delivered_priority')
							__end_hold(true)
					ID.COMPONENT_TYPE_TRAIN:
						var is_success = current_collider.add_stored_order(current_held_order_id[current_held_order_id.size() - 1])
						if is_success:
							if STATE.get_is_tutorial():
								if STATE.get_order(current_held_order_id[current_held_order_id.size() - 1]).is_fragile:
									EVENTS.emit_signal('tutorial_delivered_fragile')
								else:
									EVENTS.emit_signal('tutorial_delivered_normal')
							__end_hold(true)
					ID.COMPONENT_TYPE_SHELF:
						if juggling_active and current_held_node.size() < 5:
							var item_id = current_collider.get_item(current_collider_shape)
							if item_id != ID.ITEM_EMPTY:
								add_juggling_item(item_id)
					ID.COMPONENT_TYPE_DROP:
						if juggling_active and current_held_node.size() < 5:
							add_juggling_item(current_collider.item_id)
							current_collider.get_parent().remove_child(current_collider)
							current_collider.queue_free()
					ID.COMPONENT_TYPE_PACKAGE_STACK:
						if juggling_active and current_held_node.size() < 5:
							add_juggling_item(current_collider.current_variant)
			else:
				pass
				# drop_held_package(false) - todo
		else:
			if current_collider and 'type' in current_collider:
				match current_collider.type:
					ID.COMPONENT_TYPE_SHELF:
						var item_id = current_collider.get_item(current_collider_shape)
						if item_id != ID.ITEM_EMPTY:
							if juggling_active:
								add_juggling_item(item_id)
							else:
								start_hold_item(item_id)
							if STATE.get_is_tutorial():
								EVENTS.emit_signal('tutorial_picked_item_from_shelf')
					ID.COMPONENT_TYPE_DROP:
						if juggling_active:
							add_juggling_item(current_collider.item_id)
						else:
							start_hold_item(current_collider.item_id)
						current_collider.get_parent().remove_child(current_collider)
						current_collider.queue_free()
					ID.COMPONENT_TYPE_PACKAGE_STACK:
						if juggling_active:
							add_juggling_item(current_collider.current_variant)
						else:
							start_hold_item(current_collider.current_variant)
					ID.COMPONENT_TYPE_PACKAGING_STATION, ID.COMPONENT_TYPE_FRAGILE_STATION:
						var held_id = current_collider.get_stored_item()
						if held_id == ID.ITEM_ORDER:
							if juggling_active:
								add_juggling_item(held_id, current_collider.get_order_id())
							else:
								start_hold_order(current_collider.get_order_id())
						elif held_id != ID.ITEM_EMPTY:
							if juggling_active:
								add_juggling_item(held_id)
							else:
								start_hold_item(held_id)
	elif MULTI_INPUT.get_action_pressed_for_player(event, id):
		if current_held_item_id.size() > 0:
			drop_held_item(true)
		elif current_held_order_id.size() > 0:
			pass
			#drop_held_packge(true) - todo
		else:
			if current_collider and 'type' in current_collider:
				match current_collider.type:
					ID.COMPONENT_TYPE_PACKAGING_STATION, ID.COMPONENT_TYPE_FRAGILE_STATION:
						var is_working = current_collider.start_work(MULTI_INPUT.get_player_by_id(id).color, id)
	elif MULTI_INPUT.get_action_released_for_player(event, id):
		if current_held_item_id.size() > 0:
			pass
		else:
			if current_collider and 'type' in current_collider:
				match current_collider.type:
					ID.COMPONENT_TYPE_PACKAGING_STATION, ID.COMPONENT_TYPE_FRAGILE_STATION:
						current_collider.pause_work()
	elif MULTI_INPUT.get_powerup_pressed_for_player(event, id):
		if current_power_up:
			EVENTS.emit_signal('power_up_used', id, current_power_up)
			power_up_active = true
			match current_power_up:
				ID.POWER_UP_GHOST:
					set_collision_layer(0b00000000000000000010)
					set_collision_mask(0b00000000000000000010)
				ID.POWER_UP_PIGGYBANK:
					STATE.add_points(50)
				ID.POWER_UP_DASH:
					dash_active = true
				ID.POWER_UP_JUGGLING:
					juggling_active = true
					if current_held_node.size() > 0:
						current_held_node.pop_back().queue_free()
						if current_held_item_id.size() > 0:
							add_juggling_item(current_held_item_id.pop_back())
						else:
							add_juggling_item(ID.ITEM_ORDER, current_held_order_id.pop_back())
					$BaseItemAnchor.hide()
					$JugglingItems.show()
					__start_juggle()
			current_power_up = null

	""" - test for anim
	if event.is_action_pressed('debug_f1'):
		var new_child = Spatial.new()
		__start_hold_small(new_child)
	elif event.is_action_pressed('debug_f2'):
		var new_child = Spatial.new()
		__start_hold_medium(new_child)
	elif event.is_action_pressed('debug_f3'):
		var new_child = Spatial.new()
		__start_hold_large(new_child)
	elif event.is_action_pressed('debug_f4'):
		__start_juggle()
	elif event.is_action_pressed('debug_f5'):
		__end_hold()
	#"""


func add_juggling_item(item_id, order_id = -1):
	var item_obj
	if item_id in DATA.ITEMS:
		item_obj = DATA.ITEMS[item_id].obj.instance()
	if item_id in DATA.PACKAGE_SIZES:
		item_obj = DATA.PACKAGE_SIZES[item_id].parts_obj.instance()
	if item_id == ID.ITEM_ORDER:
		var order_data = STATE.get_order(order_id)
		var package_data = DATA.PACKAGE_SIZES[order_data.package_size]
		if order_data.is_fragile and order_data.is_priority:
			item_obj = package_data.packed_fragile_priority_obj.instance()
		elif order_data.is_fragile:
			item_obj = package_data.packed_fragile_obj.instance()
		elif order_data.is_priority:
			item_obj = package_data.packed_priority_obj.instance()
		else:
			item_obj = package_data.packed_obj.instance()
	item_obj.set_scale(Vector3(0.25, 0.25, 0.25))
	var new_pathfollow = PathFollow.new()
	new_pathfollow.set_script(path_follow_script)
	new_pathfollow.add_child(item_obj)
	$JugglingItems.add_child(new_pathfollow)
	current_held_node.append(item_obj)
	if item_id == ID.ITEM_ORDER:
		current_held_order_id.append(order_id)
	else:
		current_held_item_id.append(item_id)


func _physics_process(_delta):
	var move = MULTI_INPUT.get_move_vector_for_player(id, dash_active)
	if move != Vector3.ZERO:
		move_and_collide(move)

		set_rotation(Vector3(
			0,
			Vector3(move.x, 0, move.z * -1.0).signed_angle_to(Vector3.FORWARD, Vector3.UP),
			0
		))

		walk_animation_player.play('Walk')
	else:
		walk_animation_player.stop()
	
	var new_collider = $Selection.get_collider()
	var new_collider_shape = $Selection.get_collider_shape()
	if new_collider != current_collider or new_collider_shape != current_collider_shape:
		if new_collider == current_collider and new_collider_shape != current_collider_shape:
			if current_collider:
				if current_collider.has_method('remove_selection'):
					current_collider.remove_selection(current_collider_shape)
				if current_collider.has_method('add_selection'):
					current_collider.add_selection(new_collider_shape, id)
				current_collider_shape = new_collider_shape
		elif new_collider != current_collider:
			if current_collider and is_instance_valid(current_collider) and current_collider.has_method('remove_selection'):
				current_collider.remove_selection(current_collider_shape)
			if new_collider and is_instance_valid(new_collider) and new_collider.has_method('add_selection'):
				new_collider.add_selection(new_collider_shape, id)
				current_collider_shape = new_collider_shape
			current_collider = new_collider
			if current_collider == null:
				current_collider_shape = null


func start_hold_item(item_id):
	current_held_item_id.append(item_id)

	var item
	var item_obj
	if item_id in DATA.ITEMS:
		item = DATA.ITEMS[item_id]
		item_obj = item.obj
	elif item_id in DATA.PACKAGE_SIZES:
		item = DATA.PACKAGE_SIZES[item_id]
		item_obj = item.parts_obj
	else:
		return
	current_held_node.append(item_obj.instance())

	if item_id in DATA.ITEMS:
		if item.size > DATA.PACKAGE_SIZES[ID.PACKAGE_SIZE_PACKAGE].space:
			__start_hold_large(current_held_node[current_held_node.size() - 1])
		elif item.size > DATA.PACKAGE_SIZES[ID.PACKAGE_SIZE_PAPERBAG].space:
			__start_hold_medium(current_held_node[current_held_node.size() - 1])
		else:
			__start_hold_small(current_held_node[current_held_node.size() - 1])
	elif item_id in DATA.PACKAGE_SIZES:
		__start_hold_node_for_package(item_id)


func start_hold_order(order_id):
	current_held_order_id.append(order_id)

	var order = STATE.get_order(order_id)

	var obj_id = 'packed%s%s_obj'%[
		'_fragile' if order.is_fragile else '',
		'_priority' if order.is_priority else ''
	]
	var obj = DATA.PACKAGE_SIZES[order.package_size][obj_id]
	current_held_node.append(obj.instance())

	__start_hold_node_for_package(order.package_size)


func __start_hold_node_for_package(package_id):
	match package_id:
		ID.PACKAGE_SIZE_PAPERBAG:
			# quick fix so it's held correctly:
			current_held_node[current_held_node.size() - 1].translate(Vector3(0.0, 0.7, 0.2))
			__start_hold_small(current_held_node[current_held_node.size() - 1])
		ID.PACKAGE_SIZE_PACKAGE:
			current_held_node[current_held_node.size() - 1].translate(Vector3(0.0, 0.4, 0.25))
			__start_hold_medium(current_held_node[current_held_node.size() - 1])
		ID.PACKAGE_SIZE_CRATE:
			current_held_node[current_held_node.size() - 1].translate(Vector3(0.0, 0.9, -0.6))
			current_held_node[current_held_node.size() - 1].set_rotation(Vector3(0.0, 90.0, 0.0))
			__start_hold_large(current_held_node[current_held_node.size() - 1])

func __start_hold_small(item):
	anim_player.play('HoldSmall')
	small_item_anchor.add_child(item)

func __start_hold_medium(item):
	anim_player.play('HoldMedium')
	medium_item_anchor.add_child(item)

func __start_hold_large(item):
	anim_player.play('HoldLarge')
	large_item_anchor.add_child(item)

func __start_juggle():
	anim_player.play('Juggle', -1, 0.9)


func __end_juggle():
	anim_player.play("RESET")


func __end_hold(delete_first = false):
	if anim_player.assigned_animation != 'Juggle':
		anim_player.play_backwards()

	if small_item_anchor.get_child_count() > 0:
		var old_small_child = small_item_anchor.get_child(0)
		small_item_anchor.remove_child(old_small_child)
		old_small_child.queue_free()
	elif medium_item_anchor.get_child_count() > 0:
		var old_medium_child = medium_item_anchor.get_child(0)
		medium_item_anchor.remove_child(old_medium_child)
		old_medium_child.queue_free()
	elif large_item_anchor.get_child_count() > 0:
		var old_large_child = large_item_anchor.get_child(0)
		large_item_anchor.remove_child(old_large_child)
		old_large_child.queue_free()
	
	if juggling_active:
		if delete_first:
			current_held_node.pop_front().queue_free()
			current_held_order_id.pop_back()
		else:
			current_held_node.pop_back().queue_free()
			current_held_item_id.pop_back()
	else:
		current_held_node.pop_back()
		current_held_item_id.pop_back()
		current_held_order_id.pop_back()

func drop_held_item(yeet: bool):
	var drop = drop_scene.instance()
	drops_anchor.add_child(drop)
	if yeet:
		drop.set_item_for_yeet(current_held_item_id[current_held_item_id.size() - 1], rotation.y, current_held_node[current_held_node.size() - 1].global_translation)
	else:
		var target_pos = translation + (Vector3.BACK.rotated(Vector3.UP, rotation.y) * DROP_OFFSET)
		drop.set_item_for_drop(current_held_item_id[current_held_item_id.size() - 1], rotation.y, target_pos, current_held_node[current_held_node.size() - 1].global_translation)

	__end_hold()
