extends StaticBody


onready var marker1 = $Marker

var looking_at_pos = 0
var orders_stored = []
var type = ID.COMPONENT_TYPE_TRAIN


func _ready():
	pass


func add_selection(collision_shape_index: int, player_id: int):
	if collision_shape_index == 0:
		looking_at_pos += 1
		if looking_at_pos == 1:
			marker1.show()
			marker1.set_color_for_player(player_id)


func remove_selection(collision_shape_index: int):
	if collision_shape_index == 0:
		looking_at_pos -= 1
		if looking_at_pos == 0:
			marker1.hide()


func add_stored_order(order_index: int) -> bool:
	if STATE.get_order(order_index).is_priority:
		return false # only non priority orders are allowed in the train

	orders_stored.append(order_index)
	STATE.set_order_as_delivered(order_index)
	var order_data = STATE.get_order(order_index)
	match order_data.package_size:
		ID.PACKAGE_SIZE_PAPERBAG:
			STATE.add_points(4)
		ID.PACKAGE_SIZE_PACKAGE:
			STATE.add_points(11)
		ID.PACKAGE_SIZE_CRATE:
			STATE.add_points(28)
	return true
