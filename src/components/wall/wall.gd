extends StaticBody


onready var VARIANTS = {
	'standard': $wall_straight,
	'hide': null
}
var current_variant = 'standard'
var type = ID.COMPONENT_TYPE_WALL

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func set_variant(variant: String):
	current_variant = variant
	if current_variant == 'hide':
		$wall_straight.hide()
