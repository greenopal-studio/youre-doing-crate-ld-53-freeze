extends Control


export var id: int = 0


onready var bound_state_container = $'%BoundStateContainer'
onready var unbound_state_container = $'%UnboundStateContainer'
onready var player_no_label = $'%PlayerNoLabel'
onready var device_name_label = $'%DeviceNameLabel'
onready var color_picker_container = $'%ColorPickerContainer'
onready var remove_button = $'%RemoveButton'


func _ready():
	$'%BoundStateContainer'.hide()
	$'%UnboundStateContainer'.show()

	player_no_label.set_text('%d'%(id+1))

	$'%RemoveButton'.connect('pressed', self, '_on_remove_button_pressed')

	MULTI_INPUT.connect('players_changed', self, '_on_players_changed')

	for child in $'%ColorPickerContainer'.get_children():
		child.connect('pressed', self, '_on_color_pressed', [child.self_modulate, child])


func _on_color_pressed(color, child):
	if MULTI_INPUT.get_current_player_colors().has(color):
		child.set_pressed(false)
		return
	
	__set_selected_color(color)

func _on_remove_button_pressed():
	MULTI_INPUT.remove_player(id)
	remove_button.release_focus()


func _on_players_changed(players):
	if id >= players.size():
		$'%BoundStateContainer'.hide()
		$'%UnboundStateContainer'.show()
	else:
		$'%BoundStateContainer'.show()
		$'%UnboundStateContainer'.hide()
		device_name_label.set_text('(%s)'%MULTI_INPUT.get_device_name_for_player(id))
		if players[id].color == null:
			var player_colors = MULTI_INPUT.get_current_player_colors()
			var free_colors = []
			for child in $'%ColorPickerContainer'.get_children():
				if not player_colors.has(child.self_modulate):
					free_colors.append(child.self_modulate)
			if free_colors.size() > 0:
				__set_selected_color(free_colors[0])


func __set_selected_color(color: Color):
	for child in color_picker_container.get_children():
		if child.self_modulate.to_rgba32() == color.to_rgba32():
			child.pressed = true
		else:
			child.pressed = false
	
	MULTI_INPUT.update_player(id, color)
