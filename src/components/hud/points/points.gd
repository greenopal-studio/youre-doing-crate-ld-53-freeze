extends HBoxContainer


onready var points_label = $PointsLabel


func _ready():
	EVENTS.connect('points_changed', self, '_on_points_changed')

	_on_points_changed(0)


func _on_points_changed(points):
	points_label.set_text(UTIL.format_count(points) + '€')
