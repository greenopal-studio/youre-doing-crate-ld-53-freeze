extends Control


export var id: int


onready var progress = $'%Progress'
onready var icon_texture_rect = $'%IconTextureRect'
onready var no_powerup_label = $'%NoPowerupLabel'
onready var animation_player = $AnimationPlayer


var data = null


func _ready():
	if MULTI_INPUT.get_current_players_count() <= id:
		queue_free()
		return

	icon_texture_rect.hide()
	no_powerup_label.show()

	var color = MULTI_INPUT.get_player_by_id(id).color
	progress.tint_progress = color.lightened(0.2)
	progress.tint_under = Color(color.r, color.g, color.b, 0.25)
	progress.value = 0

	EVENTS.connect('power_up_touched', self, '_on_power_up_touched')
	EVENTS.connect('power_up_used', self, '_on_power_up_used')


func _on_power_up_touched(body, powerup):
	if !('type' in body) or body.type != ID.COMPONENT_TYPE_PLAYER or body.id != id:
		return

	if data != null:
		return

	data = DATA.POWER_UPS[powerup.get_type()]
	icon_texture_rect.texture = data.icon

	no_powerup_label.hide()
	icon_texture_rect.show()

	progress.value = 100


func _on_power_up_used(player_id, _powerup):
	if player_id != id:
		return
	
	var progress_animation = Animation.new()
	var value_track = progress_animation.add_track(Animation.TYPE_VALUE)
	progress_animation.track_set_path(value_track, "Progress:value")
	progress_animation.track_set_interpolation_type(value_track, Animation.INTERPOLATION_LINEAR)
	progress_animation.track_insert_key(value_track, 0.0, 100.0)
	progress_animation.track_insert_key(value_track, data.duration, 0.0)
	progress_animation.length = data.duration
	animation_player.add_animation('Progress', progress_animation)
	animation_player.play('Progress')
	animation_player.seek(0.0)

	yield(get_tree().create_timer(data.duration), "timeout")

	icon_texture_rect.hide()
	no_powerup_label.show()
	EVENTS.emit_signal('power_up_effect_ended', player_id, _powerup)
	data = null
