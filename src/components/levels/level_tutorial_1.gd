extends Level


func _ready():
	LEVEL_DESIGN = [
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     'Bb',  ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',    ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     'Ub',   '',     '',     'Ttb', ''],
		['FWlt', 'FWt',  'FWt',  'FWt',  'FWt',  'FWt',  'FWt',  'FWt',  'FWt',  'FWt',  'FWt',  'PbbWt','PpbWt','PcbWt','FWt',  'FWt',  'RrWt', '',     '',     'FWt',  'F',    'F',    'FWrt', '',    ''],
		['FWl',  'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    '',     'F',    'F',    'F',    'FWr',  'Ttb', ''],
		['FWl',  'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    '',    ''],
		['FWl',  'F',    'F',    'Sr',   '',     '',     'Sr',   '',     '',     'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'Ttb', ''],
		['FWl',  'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    '',    ''],
        ['FWl',  'F',    'F',    'F',    'F',    'F1',   'F',    'F',    'F2',   'F',    'F',    'F3',   'F',    'F',    'F',    'F',    'G1r',  '',     'F',    'F',    'F',    'F',    'F',    'Ttb', ''],
        ['FWl',  'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    '',    ''],
        ['FWl',  'F',    'F',    'F',    'F',    'F6',   'F',    'F',    'F5',   'F',    'F',    'F4',   'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'Ttb', ''],
        ['FWl',  'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'F',    'FWr',  '',    ''],
		['FWlhb','FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWhb', 'FWrhb','Ttb', ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',    ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     'Ttb', ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',    ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     'Ttb', ''],
		['',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',     '',    ''],
	]
	LEVEL_TOP = -12
	LEVEL_LEFT = -17
	
	ORDER_AMOUNT = 20

	AVAILABLE_ITEMS = [
		ID.ITEM_RUBBER_DUCK,
		ID.ITEM_CONTROLLER,
	]
	
	build_level_design()
	place_tutorial_items_on_shelves($Shelves)


func place_tutorial_items_on_shelves(shelf_group_node: Spatial):
	for i in shelf_group_node.get_children().size():
		for item_no in 3:
			shelf_group_node.get_child(i).add_item(AVAILABLE_ITEMS[i])
