extends StaticBody


onready var VARIANTS = {
	'standard': null,
	'start': null,
	'striped': $striped,
	'single': $single,
	'double': $double,
	'start_striped': $striped,
	'start_single': $single,
	'start_double': $double,
}
var variant = 'standard'

onready var mesh = $MeshInstance


func _ready():
	pass # Replace with function body.


func set_variant(new_variant: String):
	variant = new_variant
	if VARIANTS[new_variant] != null:
		VARIANTS[new_variant].show()
