extends Spatial


onready var show_timer = $ShowTimer


func _ready():
	show_timer.connect('timeout', self, 'hide')
	connect('visibility_changed', self, '_on_visibility_changed')


func set_error_message(text: String, show_directly: bool = false):
	$'%Text'.set_text(text)
	if show_directly:
		show()


func _on_visibility_changed():
	if is_visible_in_tree():
		show_timer.start()
